"""
Trees Module:

1. Includes Trie, BKTree, SuffixTree, and DAFSA classes
2. Includes functions to calculate Levenshtein edit distances
between words. 
3. Includes interfacing functions to pickle created objects

"""



# import numpy as np    # Module for matrices
import time             # Module for timing functions
# import networkx as nx # Potentially necessary package for graphs
import pickle


class DeprecatedTrie():  # First naive implementation, originally called Trie()

    def __init__(self):
        self.children = {}

    # Look up word in trie, return True or False.  If False, option to add word to trie.
    def look_up(self, word, add=False):     
        loc = self.children
        # Add end char for processing                       
        word = word + '\\'                      
        for char in word:
            try:                                
                loc = loc[char]                 
            # Start adding dicts to insert word if option to insert on failed lookup is 
            # to true.  Default is False
            except KeyError:
                if add == True:                 
                    loc[char] = {}              
                    loc = loc[char]             
                else:
                    return False
        # Return true if no KeyErrors encountered or if insert option set to True because
        # it has been inserted
        return True

    def delete(self, word):
        loc = self.children                     
        word = word + '\\'
        hold = self.children                    

        for char in word:
            # Check if trie branches
            if len(loc) > 1:                    
                del_key = char                  
                hold = loc                      
            try:                                
                loc = loc[char]                 
            except KeyError:
                return False                    
        del hold[del_key]
        return True                             

# ------------------------------------------------------------------------------------------------------------

class Node():
    def __init__(self, letter, parent=None, parent_str=''):
        self.key = letter                       # Node key
        self.parent = parent                    # Points to parent node 
        self.composite = parent_str + letter    # Full string/path at this node
        self.children = {}                      # Children nodes

    def add_child(self, letter):
        self.children[letter] = self.__class__(letter, self, self.composite)
        return self.children[letter]

    def level(self):
        return len(composite)

class Tries():
    def __init__(self):                                     
        self.node = None
        self.roots = {}                                     
        self.size = 0                                       
        for char in 'abcdefghijklmnopqrstuvwxyz':           
            self.roots[char] = Node(char)           

    def lookup(self, word, add=False, inspect=False):                   
    # Returns True if word is in trie
        self.node = self.roots[word[0]]                     
        if inspect == False:
            word = word + '\\'                              

        added = False                                      
        for char in word[1:]:
            if self.node.children.get(char) is not None:   
                self.node = self.node.children.get(char)   
            elif self.node.children.get(char) is None:
                if add == True:                             
                    added = True
                    self.node.add_child(char)               
                    self.node = self.node.children[char]    
                else:   
                    return False

        # Increment size when word is added
        if added == True:
            self.size += 1                                  

        return True

    # Returns specified node or None to allow for reference assignment
    def inspect(self, word):                            
        if self.lookup(word, inspect=True) == True:   # Check if word is in trie
            return self.node
        else: 
            return None

    # Deletes given word from trie if in trie, however, will return None upon a failed lookup
    def delete(self, word):
        self.node = self.roots.get(word[0])
        word = word + '\\'
        del_node = self.roots.get(word[0])
        del_key = ''

        for char in word[1:]:
            if len(self.node.children) > 1:                     # Check if trie branches
                del_key = char                                  # Store the current key to del item
                del_node = self.node                            # Move variable to reference holding dict 

            if self.node.children.get(char) is not None:            
                self.node = self.node.children.get(char)        # Regardless, move down trie

            else:
                return None                                 # Return failed delete              

        del del_node.children[del_key]

    def clear(self):            
    # Removes all trie entries                              
        for char in self.roots.keys():
            self.roots[char] = Node(char)
        self.size = 0                                           # Reset size count

    # Returns sorted str list of member words
    def alt_sort(self):
        results = []
        stack = []                                                          

        # Loops through 26 tries in sorted order for a-z in order 
        for key in sorted(list(self.roots.keys())):                             
            self.node = self.roots[key]

            # Seed the stack with root children
            # Sort keys before looping
            # Extend queue with sorted child nodes
            for key in sorted(list(self.node.children.keys())):                 
                stack.append(self.node.children[key])                           

            while len(stack) > 0:
                self.node = stack[0]
                buff = []                                                       

                # Check node keys for end char flag
                # Add to results, slice off the end char flag
                for key in sorted(list(self.node.children.keys())):
                    if self.node.children[key].key == '\\':                     
                        results.append(self.node.children[key].composite[:-1])  
                    # Else, add to buffer
                    else:
                        buff.append(self.node.children[key])                    
            
                # Insert/extend queue right after current node
                stack[1:1] = buff                                               
                stack = stack[1:]                                               

        return results

    # Wrapper function for recursive df-traversal
    # Returns sorted str list of member words, better performance than iterative sort
    def sort(self):                 

        # Recursive dft for the sort function
        def dft_sort(node, results):    
            for key in sorted(list(node.children.keys())):
                if key == '\\':                         # Check node keys for end char flag
                    results.append(node.children[key].composite[:-1])   # Add to results, slice off the end char flag
                else:
                    dft_sort(node.children[key], results)

        res = []                                                            

        # Loops through 26 tries in sorted order
        for key in sorted(list(self.roots.keys())):                             
            self.node = self.roots[key]
            dft_sort(self.node, res)

        return res

        

#---------------------------------------------------------------------------------------------------------------------
# Levenshtein edit distance between words with insertions, substitutions, and deletions as viable steps

# If given two words, will produce levenshtein distance.
# Can take a trie object and a single word and use Steve Hanov's method 
# of crawling through the trie to produce results.
# See: stevehanov.ca/blog/index.php?id=114
# Else, return None.
def levenshtein(str1=None, str2=None, tries=None):
    
    def dft_leven(node, word, prev, res):
    # Recursive function for crawling the trie

        # Use length of path at node to keep vertical position
        pos = len(node.composite)           
        current = []                        
        current.append(pos)                 
        
        # Initialize horizonal position 
        hpos = 1                            
        for char in word:
            # Check for char-char match 
            if node.key == char:            
                current.append(prev[hpos - 1])
            else:
            # Handle cost for mismatch  
                current.append(min(current[hpos - 1] + 1, prev[hpos - 1] + 1, prev[hpos] + 1))
            hpos += 1                       

        for key in sorted(list(node.children.keys())):                  
            # If not end of word, continue to recursively call passing current row as the next prev row            
            if key != '\\':      
                dft_leven(node.children[key], word, current, res)
                
            # Else, create new entry in results dictionary
            elif key == '\\':
                res[node.composite] = current[len(current) - 1]


    # Set up to make call recur. func on tries
    results = {}

    # For the word and trie case
    if tries != None and str1 != None:                          
        prev_row = [x for x in range(0, len(str1) + 1)]

        for key in sorted(list(tries.roots.keys())):
            tries.node = tries.roots[key]
            dft_leven(tries.node, str1, prev_row, results)

        return results

    # For the word-word case
    # A naive iterative solution, see: wikipedia
    # Checks for two word inputs
    elif str1 != None and str2 != None:                     

        prev_row = [x for x in range(0, len(str2) + 1)]     
        curr_row = []                                       
        vpos = 1                                            
        for char1 in str1:      
            # Introduce element of the leftmost column
            curr_row.append(vpos)                           
            hpos = 1                                        
            vpos += 1                                       
            for char2 in str2:
                if char1 == char2:
                    # No edit necessary
                    curr_row.append(prev_row[hpos - 1])     
                else:
                    # Find cheapest edit
                    curr_row.append(min(curr_row[hpos - 1] + 1, prev_row[hpos - 1] + 1, prev_row[hpos] + 1))
                
                hpos += 1
            
            # Set current row as previous row
            prev_row = curr_row

            # If at the end, return the Leven dist, else reset the current row
            if vpos != len(str1) + 1:
                curr_row = []
            else:
                return curr_row[len(curr_row) - 1]

    else: 
        return None

#----------------------------------------------------------------------------------------------------------------------
# Burkhard-Keller Tree based on edit distances serving as a metric for fuzzy string matching purposes

class BKNode(Node):
    
    def __init__(self, word, parent=None):
        self.word = word
        self.parent = parent
        self.children = {}

    def add_child(self, word, dist):
        self.children[dist] = self.__class__(word, self)
        return self.children[dist]

# See nullwords.wordpress.com, bk-tree entry
class BKTree():

    def __init__(self, word):
        self.root = BKNode(word)

    # Takes word1, which is the word I'm adding, 
    # Word2 is the current word to compare against
    # Travels nodes, node = current node
    def add_node(self, word1, word2, node):
        dist = levenshtein(str1 = word1, str2=word2)
        if dist not in node.children:
            node.add_child(word1, dist)
        elif dist in node.children:
            self.add_node(word1, node.children[dist].word, node.children[dist])
        
    # Build the tree given a list of words
    def build(self, words):
        # Given list words, add nodes per word
        for word in words:
            self.add_node(word, self.root.word, self.root)


#----------------------------------------------------------------------------------------------------------------------
# Autocomplete and Spellcheck functions based respectively on Trie and BK Tree structures

# Returns possible results if lookup fails given desired word, 
# Completes incomplete words, however, does so from last successful node lookup
def auto_comp(word, trie):

    # Iterative breadth-first search on trie nodes to perform autocomplete on failed lookups
    # Takes current node location and ceiling number of steps to terminate bfs
    def bfs(node, ceiling = 10):                                            
    
        step_count = 0      
        queue = []          
        res = []            

        # Seed the queue
        for key in sorted(list(node.children.keys())):  
            queue.append(node.children[key])        

        # Continues while queue not empty and step ceiling not reached)
        while len(queue) != 0 and step_count <= ceiling:
            if queue[0].key == '\\':
                res.append(queue[0].composite.strip('\\'))
            else:
                for key in sorted(list(queue[0].children.keys())):
                    queue.append(queue[0].children[key])

            # FIFO
            del queue[0]    
            step_count += 1 
        
        return res
    
    if trie.lookup(word) == False:
        return bfs(trie.node)
    
    else:
    # If word already exists in trie, return it in a list for consistency
        return [word]

# Takes word, typo tolerance, and a BKTree object
# Recursively goes through BK Tree
def spell_check(word, bktree, tolerance = 2):

    res = []                
    node = bktree.root      

    def child_check(node):

        key = levenshtein(word, node.word)
        if key <= tolerance:
            res.append(node.word)
        
        # Set tolerance range.  Avoid negative values.
        if (key - tolerance) < 0: 
            range_ = range(0, key + tolerance + 1)
        else:
            range_ = range(key - tolerance, key + tolerance + 1)        

        for num in range_:
            try:
                child_check(node.children[num])
            except:
                pass

    child_check(node)       
    return res

#------------------------------------------------------------------------------------------------------------

"""

class suffixTree(Node):

    def __init__(self, word):
        self.children = []


"""


def main():
    pass

if __name__ == '__main__':
    pass