import unittest
import Trees

class TrieTests(unittest.TestCase):

	def setUp(self):
		self.trie = Trees.Tries()
		self.word = 'wordsmith'
		self.subword = 'word'
		self.str1 = 'sitting'
		self.str2 = 'kitten'

	def tearDown(self):
		self.trie.clear()

	def testTrieLookupAndInsert(self):
		# Lookup False, Insert True, Lookup True, Lookup subword False
		self.assertEqual(self.trie.lookup(self.word), False)    	 		# Test lookup on empty trie
		self.assertEqual(self.trie.lookup(self.word, True), True)			# Test insert on empty trie
		self.assertEqual(self.trie.lookup(self.word), True)				# Test lookup after insert
		self.assertEqual(self.trie.lookup(self.subword), False)			# Test lookup for sub-word

	def testDelete(self):
		# Note: Bad setup, fails on insert/look up failure also.  
		self.trie.lookup(self.subword, True)								# Insert word to be deleted
		self.trie.lookup(self.word, True)									# Insert child-word to be preserved
		self.trie.delete(self.word)
		self.assertEqual(self.trie.lookup(self.word), False)				# Check if word is deleted
		self.assertEqual(self.trie.lookup(self.subword), True)				# Check if child-word is intact

	def testInspect(self):
		# Add word, inspect prefix node
		i = 6
		self.trie.lookup(self.word, True)
		self.assertEqual(self.trie.inspect(self.word[:i + 1]).key, self.word[i])

	def testClear(self):
		# Insert word, check if cleared, check size reset
		self.trie.lookup(self.word, True)
		self.trie.clear()
		self.assertEqual(self.trie.lookup(self.word), False)
		self.assertEqual(self.trie.size, 0)

	def testSort(self):
		cases = ['word', 'something', 'ant', 'aunt', 'ann', 'and', 'ban', 'wordsmith']
		
		# Insert case words
		for case in cases:
			self.trie.lookup(case, True)

		# Use built-in sort function
		cases.sort()

		# Check trie sort against built-in sort function
		for (i,case) in enumerate(cases):
			self.assertEqual(self.trie.sort()[i], case)

	def testLevenshteinTwoWords(self):
		# Testing commutivity and the known solution d(sitting, kitten) = 3
		self.assertEqual(Trees.levenshtein(str1 = self.str1, str2 = self.str2), 3)
		self.assertEqual(Trees.levenshtein(str1 = self.str2, str2 = self.str1), 3)

		

	def testDFTTrieLevenshtein(self):
		cases = ['word', 'something', 'ant', 'aunt', 'ann', 'and', 'ban', 'wordsmith', 'kitten', 'sitting']
		
		# Insert case words
		for case in cases:
			self.trie.lookup(case, True)

		# Assign dict of results for 'kitten' and the rest of the trie
		res = Trees.levenshtein(str1 = self.str1, tries = self.trie)

		# Loop through naive Leven distance calculations and compare to results in dict
		for case in cases:

			dist = Trees.levenshtein(str1 = self.str1, str2 = case)
			print(self.str1, case, dist)
			self.assertEqual(dist, res[case])





"""
class TrieLevenDistanceTests(unittest.TestCase): 

	def setUp(self):
		self.trie = Trie.Trie()
		words = [asd, asdf, asdfg, apple, word, wordsmith, ]
		for word in words:
			self.trie.lookup(word, True) 									# Insert the words to be distanced

	def tearDown(self):
		pass

	def testLevenshteinDistance(self):
		for (word1, word2, dist) in values:
			self.trie.look_up(word1)
			self.trie.look_up(word2)
			self.assertEqual(self.trie.leven_dist(word1, word2), dist)
"""

if __name__ == '__main__':

	unittest.main()

